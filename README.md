Downloads postage stamp images from PS1 DR1. To see how to run the code run

```bash
python grab_ps1_postage_stamp.py -h
```