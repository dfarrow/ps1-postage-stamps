"""
Grab a PS1, PV3 postage stamp image. Uses the requests 
module from

http://requests.readthedocs.io/en/master/

for usage imformation run python grab_ps1_postage_stamp.py -h 

AUTHORS:

Daniel Farrow 2017 (MPE)
"""
from __future__ import print_function

import six
from six import iteritems
from collections import OrderedDict 
import argparse
from PIL import Image, ImageDraw
from io import BytesIO
import requests

def read_in_arguments():
    """
    Read in the command line arguments

    Returns
    -------
    args : argparse.Namespace
        the parsed arguments
    """
    parser = argparse.ArgumentParser(description="Download PS1 3pi postage stamps")
    parser.add_argument('ra', type=float, help="RA in decimal degrees") 
    parser.add_argument('dec', type=float, help="DEC in decimal degrees")  
    parser.add_argument('--mark', action='store_true', help="Add an ellipse to the image center")
    parser.add_argument('--invert', action='store_true', help="Invert the image scale")
    parser.add_argument('--outbase', type=str, help="A suffix to append to output filenames", default="psstamp_")
    parser.add_argument('--size', type=int, help="Size of postage stamp in pixels", default="128")
    parser.add_argument('--bands', type=str, help="""An unseparated 
                        list of up to three bands to use to form an image, 
                        e.g. r, gr, riz .. etc""", 
                        default="gri")
    
    args = parser.parse_args()
    
    return args


def get_skycell_dict(ra, dec, bands):
    """
    Request a list of skycells at the
    given position and in the given bands
    from 

    http://ps1images.stsci.edu/cgi-bin/ps1filenames.py

    Parameters
    ----------
    ra, dec : float
       the right ascension and declination in degrees
    bands : str
       the desired bands

    Returns
    -------
    fimages : dict
        a dictionary of band, image filename pairs
    """

    payload = {'ra' : ra, 'dec' : dec, 'filters' : bands}
    r = requests.get('http://ps1images.stsci.edu/cgi-bin/ps1filenames.py', 
                     params=payload)

    if r.status_code != requests.codes.ok:
        r.raise_for_status()
  
    fimages = {}
    if six.PY2:
        content = r.content
    else:
        content = r.content.decode()
    for line in content.splitlines():

        els = line.split()

        # skip header line and empty lines
        if "projcell" in line or len(els) < 8:
            continue

        fimages[els[4]] = els[7]
       
    return fimages

def get_postage_stamp(ra, dec, skycelld, size=512, invert=False):
    """
    Return a postage stamp image at the 
    location given, of the skycells given 
    in skycelld. For a colour images, pass
    multiple bands in skycelld

    Parameters
    ----------
    ra, dec : float
       right ascension and declination of 
       postage stamp
    skycelld : dict
       dictionary of band, skycell pairs
    size : int (optional)
       the cutout size in pixels
 
    Returns
    -------
    img : PIL Image
        the postage stamp
    used_bands : list
        the bands used for Red, Green and Blue
    """

    #
    bands = ["Red", "Green", "Blue"] 

    # dictionary to sort out which band to put in which
    # RGB channel 
    redness = {'g' : 1, 'r' : 2, 'i' : 3, 'z' : 4,
               'y' : 5, 'w' : 6}

    # Ordered dict just to make it easier to read
    payload=OrderedDict([('RA', ra),('Dec', dec), ('Size',size), ('Invert', invert)])

    # sort the bands by how red they are 
    sorted_skycelld = OrderedDict(sorted(iteritems(skycelld), 
                                  key = lambda r : redness[r[0]],
                                  reverse=True))

    # Allocate images to Red, Green, Blue channels
    used_bands = []
    for ib, (band, image) in enumerate(iteritems(sorted_skycelld)):

        if band not in "grizyw":
            raise ValueError("Bad %s not recognized"%(band))

        if ib > 2:
            print("WARNING! Found more bands or images than needed for colour image")            
            break

        payload[bands[ib]] = image
        used_bands.append(band)

    payload_str = "&".join(["{:s}={:s}".format(x, str(y)) for x, y in iteritems(payload)])

    # Get the image
    imager = requests.get("http://ps1images.stsci.edu/cgi-bin/fitscut.cgi", params=payload_str)

    if imager.status_code != requests.codes.ok:
        imager.raise_for_status()

    # Convert it to an image
    img = Image.open(BytesIO(imager.content))
    
    return img, used_bands

def add_ellipse(img, size=16):
    """
    Add an ellipse around the
    center of the img

    Parameters
    ----------
    img : PIL Image 

    size : float
        the diameter of the circle

    """
    draw = ImageDraw.Draw(img)

    #left, upper, right, lower
    bounding_box = (img.size[0]/2 - size/2, img.size[0]/2 - size/2,
                    img.size[1]/2 + size/2, img.size[1]/2 + size/2)
    draw.ellipse(bounding_box, outline="#ff0000")
    del draw


args = read_in_arguments()
# find the skycells in this region
skycelld = get_skycell_dict(args.ra, args.dec, args.bands) 

# request the postage stamp
img, used_bands = get_postage_stamp(args.ra, args.dec, skycelld, size=args.size, invert=args.invert) 

# add an ellipse to show the center
if args.mark:
    add_ellipse(img)

# save the image
filename = "{:s}{:5.2f}_{:4.2f}_{:s}.png".format(args.outbase, args.ra, args.dec, 
                                                 ''.join(used_bands))
img.save(filename)






